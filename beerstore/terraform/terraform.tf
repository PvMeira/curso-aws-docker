terraform {
  backend "s3" {
    bucket = "pvmeira-terraform-state"
    key = "beerstore-curso-online"
    region = "us-east-1"
    profile = "terraform"
  }
}