package com.br.beerstore.error;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Locale;

@Order
@RestControllerAdvice
@Slf4j
public class GeneralExceptionHandler {

    @Autowired
    private ApiExceptionHandler apiExceptionHandler;




    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleInternalServerError(Exception e, Locale locale) {
        log.error(" A unexpected error has occur in the system    ", e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR, apiExceptionHandler.toApiError("generic-internal-error", locale)));
    }

}
