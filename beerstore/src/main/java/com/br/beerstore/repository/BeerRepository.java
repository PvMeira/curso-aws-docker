package com.br.beerstore.repository;

import com.br.beerstore.model.Beer;
import com.br.beerstore.model.domain.BeerType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BeerRepository extends JpaRepository<Beer , Long> {

    Optional<Beer> findByNameAndType(String name, BeerType type);
}
