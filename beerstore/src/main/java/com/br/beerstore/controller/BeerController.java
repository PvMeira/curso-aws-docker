package com.br.beerstore.controller;

import com.br.beerstore.model.Beer;
import com.br.beerstore.model.dto.BeerDTO;
import com.br.beerstore.service.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/beers")
public class BeerController {

    private final BeerService beerService;

    @Autowired
    public BeerController(BeerService beerService) {
        this.beerService = beerService;
    }


    @GetMapping
    public List<Beer> listAll() {
        return this.beerService.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BeerDTO save(@Valid @RequestBody BeerDTO beer){
        return BeerDTO.toDto(this.beerService.save(BeerDTO.toEntity(beer)));
    }

    @PutMapping
    @RequestMapping("/{id}")
    public BeerDTO update(@PathVariable(name = "id")Long id, @Valid @RequestBody BeerDTO beerDTO) {
        beerDTO.setId(id);
        return BeerDTO.toDto(this.beerService.update(BeerDTO.toEntity(beerDTO)));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        this.beerService.remove(id);
        return ResponseEntity.noContent().build();
    }
}
