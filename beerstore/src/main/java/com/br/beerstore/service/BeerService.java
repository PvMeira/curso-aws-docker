package com.br.beerstore.service;

import com.br.beerstore.model.Beer;
import com.br.beerstore.model.domain.BeerType;
import com.br.beerstore.repository.BeerRepository;
import com.br.beerstore.service.exception.BeerAlreadyExistException;
import com.br.beerstore.service.exception.BeerNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Service
public class BeerService {


    private final BeerRepository beerRepository;

    @Autowired
    public BeerService(BeerRepository beerRepository) {
        this.beerRepository = beerRepository;
    }

    public Beer save(final Beer beer) {
        if (this.findByNameAndType(beer.getName(), beer.getType()).isPresent()) {
            throw new BeerAlreadyExistException();
        }
        return this.beerRepository.save(beer);
    }


    public Optional<Beer> findByNameAndType(@NotBlank String name, @NotNull BeerType type) {
        return this.beerRepository.findByNameAndType(name, type);
    }

    public List<Beer> findAll() {
        return this.beerRepository.findAll();
    }

    public Beer update(@NotNull Beer beerToBeEdited) {
        this.verifyIfBeerExists(beerToBeEdited);
        return this.beerRepository.save(beerToBeEdited);

    }

    public void remove(@NotNull Long id) {
        if (!this.beerRepository.findById(id).isPresent()) {
            throw  new BeerNotFoundException();
        }
        this.beerRepository.deleteById(id);

    }

    private void verifyIfBeerExists(final Beer beer) {
        Optional<Beer> beerByNameAndType = findByNameAndType(beer.getName(), beer.getType());
        if (beerByNameAndType.isPresent() && (beer.isNew() || isUpdatingToADifferentBeer(beer, beerByNameAndType))) {
            throw new BeerAlreadyExistException();
        }
    }

    private boolean isUpdatingToADifferentBeer(Beer beer, Optional<Beer> beerByNameAndType) {
        return beer.alreadyExist() && !beerByNameAndType.get().equals(beer);

    }

}
