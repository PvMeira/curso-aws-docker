package com.br.beerstore.service.exception;

import org.springframework.http.HttpStatus;

public class BeerAlreadyExistException extends ApplicationException {

    public BeerAlreadyExistException() {
        super("beer-already-exist", HttpStatus.BAD_REQUEST);
    }
}
