package com.br.beerstore.service.exception;

import org.springframework.http.HttpStatus;

public class BeerNotFoundException extends ApplicationException {

    public BeerNotFoundException() {
        super("beer-not-found", HttpStatus.NOT_FOUND);
    }
}
