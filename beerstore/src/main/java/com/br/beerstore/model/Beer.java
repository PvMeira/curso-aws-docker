package com.br.beerstore.model;

import com.br.beerstore.model.domain.BeerType;
import com.br.beerstore.model.domain.converter.BeerTypeConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Beer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    @Column(name = "ID")
    private Long id;
    @NotBlank(message = "beer-name-notblank")
    @Column(name = "NAME")
    private String name;
    @NotNull(message = "beer-type-notnull")
    @Column(name = "TYPE")
    @Convert(converter = BeerTypeConverter.class)
    private BeerType type;
    @NotNull(message = "beer-volume-notnull")
    @DecimalMin(value = "0", message = "beer-volume-decimalmin")
    @Column(name = "VOLUME")
    private BigDecimal volume;



    @JsonIgnore
    public Boolean isNew() {
        return this.getId() == null;
    }

    @JsonIgnore
    public Boolean alreadyExist() {
        return this.getId() != null;
    }

}
