package com.br.beerstore.model.dto;

import com.br.beerstore.model.Beer;
import com.br.beerstore.model.domain.BeerType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode
public class BeerDTO {

    private Long id;
    @NotBlank(message = "beer-name-notblank")
    private String name;
    @NotNull(message = "beer-type-notnull")
    private BeerType type;
    @NotNull(message = "beer-volume-notnull")
    @DecimalMin(value = "0", message = "beer-volume-decimalmin")
    private BigDecimal volume;

    @JsonIgnore
    public static BeerDTO toDto(Beer beer) {
        BeerDTO beerDTO = new BeerDTO();
        beerDTO.setId(beer.getId());
        beerDTO.setName(beer.getName());
        beerDTO.setType(beer.getType());
        beerDTO.setVolume(beer.getVolume());
        return beerDTO;
    }

    @JsonIgnore
    public static Beer toEntity(BeerDTO beerDTO) {
        Beer beer = new Beer();
        beer.setId(beerDTO.getId());
        beer.setName(beerDTO.getName());
        beer.setType(beerDTO.getType());
        beer.setVolume(beerDTO.getVolume());
        return beer;
    }

}
