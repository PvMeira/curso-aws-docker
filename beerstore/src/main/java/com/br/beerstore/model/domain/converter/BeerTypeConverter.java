package com.br.beerstore.model.domain.converter;

import com.br.beerstore.model.domain.BeerType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Optional;

@Converter
public class BeerTypeConverter implements AttributeConverter<BeerType, String> {

    @Override
    public String convertToDatabaseColumn(BeerType attribute) {
        return attribute.getValue();
    }

    @Override
    public BeerType convertToEntityAttribute(String dbData) {
        Optional<BeerType> beerType = BeerType.getEnumValue(dbData);
        return beerType.isPresent() ? beerType.get() : null;
    }
}
