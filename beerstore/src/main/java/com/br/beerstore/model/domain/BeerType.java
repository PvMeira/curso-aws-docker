package com.br.beerstore.model.domain;

import java.util.Optional;

public enum BeerType {

    LAGGER("LAGGER", "Tipo Lagger"),
    ALE("ALE", "Tipo Ale"),
    STOUT("STOUT", "Tipo Stout"),
    PORTER("PORTER", "Tipo Porter"),
    MALT("MALT", "TIpo malt");


    private String value;
    private String description;

    BeerType(String value, String description) {
        this.value = value;
        this.description = description;
    }


    public static Optional<BeerType> getEnumValue(String value) {
        for (BeerType statusType : BeerType.values()) {
            if (statusType.getValue().equals(value))
                return Optional.of(statusType);
        }
        return Optional.empty();
    }

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    }
