package com.br.beerstore.service;

import com.br.beerstore.model.Beer;
import com.br.beerstore.model.domain.BeerType;
import com.br.beerstore.repository.BeerRepository;
import com.br.beerstore.service.exception.BeerAlreadyExistException;
import com.br.beerstore.service.exception.BeerNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Optional;

public class BeerServiceTest {


    @Mock
    private BeerRepository beerRepositoryMock;

    private BeerService beerService;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        beerService = new BeerService(beerRepositoryMock);
    }

    @Test(expected = BeerAlreadyExistException.class)
    public void should_deny_creation_of_bear_that_exists() {
        Beer databaseBeer = new Beer();
        databaseBeer.setId(2L);
        databaseBeer.setName("Teste Cerveja 3");
        databaseBeer.setType(BeerType.LAGGER);
        databaseBeer.setVolume(new BigDecimal("8989.9"));
        when(beerRepositoryMock.findByNameAndType("Teste Cerveja 3", BeerType.LAGGER)).thenReturn(Optional.of(databaseBeer));

        Beer databaseBeer1 = new Beer();
        databaseBeer1.setId(2L);
        databaseBeer1.setName("Teste Cerveja 3");
        databaseBeer1.setType(BeerType.LAGGER);
        databaseBeer1.setVolume(new BigDecimal("8989.9"));
        this.beerService.save(databaseBeer1);
    }

    @Test
    public void should_create_new_beer() {
        Beer beer = new Beer();
        beer.setName("Teste Cerveja 4");
        beer.setType(BeerType.LAGGER);
        beer.setVolume(new BigDecimal("8989.9"));


        Beer databaseBeerSave = new Beer();
        databaseBeerSave.setName("Teste Cerveja 4");
        databaseBeerSave.setType(BeerType.LAGGER);
        databaseBeerSave.setVolume(new BigDecimal("8989.9"));

        Beer databaseBeerSaveReturn = new Beer();
        databaseBeerSaveReturn.setId(1L);
        databaseBeerSaveReturn.setName("Teste Cerveja 3");
        databaseBeerSaveReturn.setType(BeerType.LAGGER);
        databaseBeerSaveReturn.setVolume(new BigDecimal("8989.9"));


        when(beerRepositoryMock.save(databaseBeerSave)).thenReturn(databaseBeerSaveReturn);

        Beer save = this.beerService.save(beer);
        assertThat(save.getId(), equalTo(1L));
        assertThat(save.getName(), equalTo("Teste Cerveja 3"));
        assertThat(save.getType(), equalTo(BeerType.LAGGER));
        assertThat(save.getVolume(), equalTo(new BigDecimal("8989.9")));

    }

    @Test
    public void should_update_existing_beer() {
        final Beer beerInDatabase = new Beer();
        beerInDatabase.setId(10L);
        beerInDatabase.setName("Devassa");
        beerInDatabase.setType(BeerType.LAGGER);
        beerInDatabase.setVolume(new BigDecimal("300"));

        when(beerRepositoryMock.findByNameAndType("Devassa", BeerType.LAGGER)).thenReturn(Optional.of(beerInDatabase));

        final Beer beerToUpdate = new Beer();
        beerToUpdate.setId(10L);
        beerToUpdate.setName("Devassa");
        beerToUpdate.setType(BeerType.LAGGER);
        beerToUpdate.setVolume(new BigDecimal("200"));

        final Beer beerMocked = new Beer();
        beerMocked.setId(10L);
        beerMocked.setName("Devassa");
        beerMocked.setType(BeerType.LAGGER);
        beerMocked.setVolume(new BigDecimal("200"));

        when(beerRepositoryMock.save(beerToUpdate)).thenReturn(beerMocked);

        Beer beerUpdated = this.beerService.update(beerToUpdate);

        assertThat(beerUpdated.getId(), equalTo(10L));
        assertThat(beerUpdated.getName(), equalTo("Devassa"));
        assertThat(beerUpdated.getType(), equalTo(BeerType.LAGGER));
        assertThat(beerUpdated.getVolume(), equalTo(new BigDecimal("200")));

    }

    @Test(expected = BeerAlreadyExistException.class)
    public void should_deny_update_of_an_existing_beer_that_already_exists() {
        final Beer beerInDatabase = new Beer();
        beerInDatabase.setId(10L);
        beerInDatabase.setName("Heineken");
        beerInDatabase.setType(BeerType.LAGGER);
        beerInDatabase.setVolume(new BigDecimal("355"));

        when(beerRepositoryMock.findByNameAndType("Heineken", BeerType.LAGGER)).thenReturn(Optional.of(beerInDatabase));

        final Beer beerToUpdate = new Beer();
        beerToUpdate.setId(5L);
        beerToUpdate.setName("Heineken");
        beerToUpdate.setType(BeerType.LAGGER);
        beerToUpdate.setVolume(new BigDecimal("355"));

        beerService.save(beerToUpdate);
    }


    @Test(expected = BeerNotFoundException.class)
    public void should_deny_delete_of_non_existing_beer() {
        when(beerRepositoryMock.findById(1L)).thenReturn(Optional.empty());
        this.beerService.remove(1L);
    }

    @Test
    public void should_remove_existing_beer() {
        final Beer beerInDatabase = new Beer();
        beerInDatabase.setId(10L);
        beerInDatabase.setName("Heineken");
        beerInDatabase.setType(BeerType.LAGGER);
        beerInDatabase.setVolume(new BigDecimal("355"));
        when(beerRepositoryMock.findById(10L)).thenReturn(Optional.of(beerInDatabase));
        this.beerService.remove(10L);
    }
}
